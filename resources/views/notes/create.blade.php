@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ url('notes') }}" class="btn btn-sm btn-info">&larr; List of Your Notes</a>
            <div class="clearfix"><br/></div>
            <div class="panel panel-default">
                <div class="panel-heading">Add New Notes!</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                      <form method="post" action="{{ url('notes') }}">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Title">
                            </div>
                            
                            <div class="form-group">
                                <label>Priority</label>
                                <select name="priority" class="form-control">
                                    <option value="Important">Important</option>
                                    <option value="Urgent">Urgent</option>
                                    <option value="Normal">Normal</option>
                                    <option value="Less-Priority">Less-Priority</option>
                                    
                                </select>
                            </div>
                        
                           
                            <div class="form-group">
                                <label>Color</label>
                                <select name="color" class="form-control">
                                    <option value="Yelllow">Yellow</option>
                                    <option value="Pink">Pink</option>
                                    <option value="#80cbc4">Green</option>
                                    <option value="#b39ddb">Purple</option>
                                    <option value="#ffcc80">Orange</option>
                                    <option value="#b39ddb">Blue</option>
                                    
                                </select>
                            </div>
                            
                           

                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control" placeholder="Description"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Image URL</label>
                                <input type="text" name="image_url" class="form-control" placeholder="Image URL">
                            </div>
                            

                            {{ csrf_field() }}
                            
                            <button type="submit" class="btn btn-primary">Save</button>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection