@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Secret Keeper!
                    <a href="{{ url('notes/create') }}" class="btn btn-primary btn-sm pull-right">Add New Note!</a>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body" >
                    <div class="row"  >
                        @foreach ($notes as $note)
                        <div class="col-sm-6 col-md-4" >
<div class="thumbnail" style="background-color : {{$note->color}}">
                                <img src="{{ $note->image_url }}" alt="{{ $note->title }}">
                                <div class="caption">
                                <p>{{ $note->created_at }}</p>
                                    <h3>{{ $note->title }}</h3>
                                    <p>{{ $note->description }}</p>
                                   
                                    <p><a href="{{ url('notes/'.$note->id.'/edit') }}" class="btn btn-primary btn-sm " role="button">Edit</a> 
                                    <a href="{{ url('notes/'.$note->id.'/delete') }}" class="btn btn-danger btn-sm" role="button">Done!</a>
                                       {{ $note->priority }}!</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
