@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ url('notes') }}" class="btn btn-sm btn-info">&larr; List of Notes!</a>
            <div class="clearfix"><br/></div>
            <div class="panel panel-default">
                <div class="panel-heading">Update notes</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                      <form method="post" action="{{ url('notes') }}">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Title" value="{{ $note->title }}">
                            </div>
                            
                            <div class="form-group">
                                <label>Priority</label>
                                <select name="priority" class="form-control">
                                    <option value="Important" {{ $note->genre == 'Important' ? 'selected' : '' }}>Important</option>
                                    <option value="Urgent" {{ $note->genre == 'Urgent' ? 'selected' : '' }}>Urgent</option>
                                    <option value="Normal" {{ $note->genre == 'Normal' ? 'selected' : '' }}>Normal</option>
                                    <option value="Less-Priority" {{ $note->genre == 'Less-Priority' ? 'selected' : '' }}>Less-Priority</option>
                                    
                                </select>
                            </div>
                        
                            <div class="form-group">
                                <label>Color</label>
                                <select name="color" class="form-control">
                                    <option value="Yelllow" {{ $note->color == 'Yellow' ? 'selected' : '' }}>Yellow</option>
                                    <option value="Pink" {{ $note->color == 'Pink' ? 'selected' : '' }}>Pink</option>
                                    <option value="#80cbc4" {{ $note->color == '#80cbc4' ? 'selected' : '' }}>Green</option>
                                    <option value="#b39ddb" {{ $note->color == '#b39ddb' ? 'selected' : '' }}>Purple</option>
                                    <option value="#ffcc80" {{ $note->color == '#ffcc80' ? 'selected' : '' }}>Orange</option>
                                    <option value="#b39ddb" {{ $note->color == '#b39ddb' ? 'selected' : '' }}>Blue</option>
                                    
                                </select>
                            </div>
                            
                            

                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control" placeholder="Description"> {{ $note->description }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Image URL</label>
                                <input type="text" name="image_url" class="form-control" placeholder="Image URL" value="{{ $note->image_url }}">
                            </div>
                            
                            

                            <input type="hidden" name="note_id" value="{{ $note->id }}" />
                            {{ csrf_field() }}

                            <button type="submit" class="btn btn-primary">Save</button>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection