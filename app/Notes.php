<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    protected $table = "notes";
    protected $fillable = [
        'user_id', 'title', 'priority', 'description', 'pages','color', 'image_url','created_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
