<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Notes;

class NotesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = \Auth::user()->books;
        return view('notes.index', compact('notes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $messages = [
            'title.required'   =>  'You must enter the title.',
            'color.required'  =>  'Enter the color.',
            'priority.required'  =>  'Enter the priority of the note.',
            'image_url.required'  =>  'Image URL required.',
            'description.required'  =>  'Enter the body of the note!',
        ];

        $this->validate($request, [
            'title' => 'required|max:256',
        ], $messages);

        if ($noteId = $request->get('book_id')) {
            $note = Notes::find($noteId);
        } else {
            $note = new Notes;
        }
        
        $note->user_id = \Auth::user()->id;
        $note->title = $request->get('title');
        $note->priority = $request->get('priority');
        $note->description = $request->get('description');
        $note->color = $request->get('color');
        $note->pages = 0;
        $note->image_url = $request->get('image_url');
        $note->save();
        

        return redirect('notes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = Notes::find($id);

        return view('notes.edit', compact('note'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Notes::find($id)->delete();
        return redirect('notes');
    }
}
