<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Notes;

class NoteController extends Controller
{
    protected $messages;
    protected $rules;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.basic');

        $this->messages = [
            'title.required'   =>  'You must enter the title.',
            'color.required'  =>  'Enter the color.',
            'priority.required'  =>  'Enter the priority of the note.',
            'image_url.required'  =>  'Image URL required.',
            'description.required'  =>  'Enter the body of the note!',
        ];

        $this->rules = [
            'title' => 'required|max:256',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \Auth::user()->books->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->messages);

        if ($validator->fails()) {
            $response['errors'] = $validator->messages();
        } else {
            $note = new Notes;

            $note->user_id = \Auth::user()->id;
            $note->title = $request->get('title');
            $note->genre = $request->get('genre');
            $note->description = $request->get('description');
            $note->author = $request->get('author');
            $note->publisher = $request->get('publisher');
            $note->pages = $request->get('pages');
            $note->image_url = $request->get('image_url');
            $note->purchase_url = $request->get('purchase_url');

            $note->save();

            $response = ['message' => 'New notes deatils successfully saved.'];
        }
        
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->messages);

        if ($validator->fails()) {
            $response['errors'] = $validator->messages();
        } else {
            $note = Notes::find($id);

            if (!$book) {
                return response()->json(['error' => 'Invalid request, note not found in records.'], 204);
            }

            $note->user_id = \Auth::user()->id;
            $note->title = $request->get('title');
            $note->genre = $request->get('genre');
            $note->description = $request->get('description');
            $note->author = $request->get('author');
            $note->publisher = $request->get('publisher');
            $note->pages = $request->get('pages');
            $note->image_url = $request->get('image_url');
            $note->purchase_url = $request->get('purchase_url');

            $note->save();

            $response = ['message' => 'Book deatils successfully updated.'];
        }
        
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Notes::find($id);
        
        if (!$note) {
            return response()->json(['error' => 'Invalid request, book not found in records.'], 204);
        } else {
            $note->delete();
            return response()->json(['message' => 'Book has been deleted successfully.']);
        }
    }
}
